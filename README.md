# README #

Perficient Google Practice Maven repository, primarily for Google GSA code that is not already in a more prominent Maven repository.

### What is this repository for? ###

* Enable Maven references to Google GSA code that is not already available on Maven's "Central Repository" or other public repositories

### How do I get set up? ###

To use the dependencies here, first you need to include the repository in your pom file:


```
#!xml

	<repositories>
		<repository>
			<id>mvn-perficient-google</id>
			<url>https://bitbucket.org/googlesearch/mvn-perficient-google/raw/master/</url>
		</repository>
	</repositories>
```


Then, just add a dependency to the `<dependencies>` section of your POM:

```
#!xml

			<dependency>
				<groupId>perficient-google</groupId>
				<artifactId>gdata-gsa</artifactId>
				<version>1.0</version>
			</dependency>
```

### Contribution guidelines ###

* Try not to duplicate files that can already be found on Maven's Central Repository http://search.maven.org/ or another public repository.
* Try to include source jars and javadoc zips when available.
* To contribute, first check out the project to your local workspace
* You may add relevant code with the mvn install:install-file command, run at the root level of your downloaded repository. Sample command:


```
#!cmd

mvn install:install-file -DlocalRepositoryPath=. -DcreateChecksum=true -Dpackaging=jar -Dfile=SOMEPATH\adaptor-4.0.4.jar  -Dsources=SOMEPATH\adaptor-4.0.4-src.jar -Djavadoc=SOMEPATH\adaptor-4.0.4-docs.zip -DpomFile=SOMEPATH\plexi-adapter-4.0.4.pom -DgroupId=perficient-google -DartifactId=plexi-adapter -Dversion=4.0.4
```
(Note that the -DpomFile is optional; otherwise one will be generated.  Use -Dsources and -Djavaddoc only if you have them.)

* for documentation see on mvn install see (https://maven.apache.org/plugins/maven-install-plugin/)
* After you make changes, commit and push back into git/bitbucket to this repository.

### Who do I talk to? ###

* Cody Coggins (cody dot coggins at perficient dot com)
* Mike Ree
* Other community or team contact